# aubo-ros-demo

## Getting started

Install aubo_robot repository for ROS Noetic from github
Use the provided instructions to install packages and then run moveit_planning_execution.launch

Launch this package using `move_test.launch` and control via Rviz 'next' button.

