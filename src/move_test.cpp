#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>
#include <tf/LinearMath/Quaternion.h>


int main(int argc, char** argv)
{
    ros::init(argc, argv, "move_test");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(1);
    spinner.start();

    static const std::string PLANNING_GROUP = "manipulator_i5";

    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    auto plan_ids = move_group.getDefaultPlannerId(PLANNING_GROUP);
    auto plan_params = move_group.getPlannerParams(plan_ids, PLANNING_GROUP);
    move_group.setPoseReferenceFrame("base_link");

    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    const robot_state::JointModelGroup* joint_model_group = move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

    auto plan_id = move_group.getPlannerId().c_str();
    ROS_INFO("Planner id: %s", plan_id);

    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("base_link");
    visual_tools.deleteAllMarkers();

    visual_tools.loadRemoteControl();

    Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
    text_pose.translation().z() = 1.2;
    visual_tools.publishText(text_pose, "KINALI move test", rvt::RED, rvt::XLARGE);
    visual_tools.trigger();

    ROS_INFO_NAMED("Kinali move test", "Planning frame: %s", move_group.getPlanningFrame().c_str());
    ROS_INFO_NAMED("Kinali move test", "End effector link: %s", move_group.getEndEffectorLink().c_str());

    // TABLE DEFINITION

    auto table_width = 1.08;
    auto table_length = 1.52;
    auto table_height = 1.01;
    auto robot_base_width = 0.35;
    auto robot_base_length = 0.44;
    auto robot_base_height = 0.09;

    auto scene_robot_height = 0.502;

    moveit_msgs::CollisionObject collision_object;
    collision_object.header.frame_id = "base_link";
    collision_object.id = "table";

    shape_msgs::SolidPrimitive table;
    table.type = table.BOX;
    table.dimensions.resize(3);
    table.dimensions[table.BOX_X] = table_length;
    table.dimensions[table.BOX_Y] = table_width;
    table.dimensions[table.BOX_Z] = table_height;

    geometry_msgs::Pose table_pose;
    table_pose.orientation.w = 1.0;
    table_pose.position.x = 0.01;
    table_pose.position.y = -0.165;
    table_pose.position.z = -table_height/2-robot_base_height/2;

    shape_msgs::SolidPrimitive robot_base;
    robot_base.type = robot_base.BOX;
    robot_base.dimensions.resize(3);
    robot_base.dimensions[robot_base.BOX_X] = robot_base_length;
    robot_base.dimensions[robot_base.BOX_Y] = robot_base_width;
    robot_base.dimensions[robot_base.BOX_Z] = robot_base_height;

    geometry_msgs::Pose robot_base_pose;
    robot_base_pose.orientation.w = 1.0;
    robot_base_pose.position.x = 0;
    robot_base_pose.position.y = -0.05;
    robot_base_pose.position.z = -robot_base_height/2;

    collision_object.primitives.push_back(table);
    collision_object.primitives.push_back(robot_base);
    collision_object.primitive_poses.push_back(table_pose);
    collision_object.primitive_poses.push_back(robot_base_pose);
    collision_object.operation = collision_object.ADD;
    std::vector<moveit_msgs::CollisionObject> collision_objects = { collision_object };

    planning_scene_interface.addCollisionObjects(collision_objects);

    visual_tools.prompt("Press 'next'");
    planning_scene_interface.removeCollisionObjects(std::vector<std::string>{collision_object.id});
}